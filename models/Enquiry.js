var keystone = require('keystone');
var Types = keystone.Field.Types;
var Email = require('keystone-email');
var moment = require('moment');

/**
 * Enquiry Model
 * =============
 */

var Enquiry = new keystone.List('Enquiry', {
	nocreate: true,
	noedit: true,
});

Enquiry.add({
	name: { type: Types.Name, required: true },
	email: { type: Types.Email, required: true },
	phone: { type: String },
	enquiryType: { type: Types.Select, options: [
		{ value: 'message', label: 'Just leaving a message' },
		{ value: 'question', label: 'I\'ve got a question' },
		{ value: 'other', label: 'Something else...' },
	] },
	message: { type: Types.Markdown, required: false },
	createdAt: { type: Date, default: Date.now },
});

Enquiry.schema.pre('save', function (next) {
	this.wasNew = this.isNew;
	next();
});

Enquiry.schema.post('save', function () {
	if (this.wasNew) {
		this.sendNotificationEmail();
	}
});

Enquiry.schema.methods.OLDsendNotificationEmail = function (callback) {
	if (typeof callback !== 'function') {
		callback = function () {};
	}
	var enquiry = this;
	keystone.list('User').model.find().where('isAdmin', true).exec(function (err, admins) {
		if (err) return callback(err);
		new keystone.Email({
			templateExt: 'hbs',
			templateEngine: require('express-handlebars'),
			templateName: 'enquiry-notification',
		}).send({
			to: admins,
			from: {
				name: 'vibezKS',
				email: 'contact@vibezks.com',
			},
			subject: 'New Enquiry for vibezKS',
			enquiry: enquiry,
		}, callback);
	});
};

Enquiry.schema.methods.sendNotificationEmail = function (callback) {
	if (typeof callback !== 'function') {
		callback = function () {};
	}
	var enquiry = this;
	enquiry.dateHuman = moment(enquiry.createdAt).format("HH:mm — DD MMMM YYYY");
	keystone.list('User').model.find().where('isAdmin', true).exec(function (err, admins) {
		if (err) return callback(err);
		new Email('./templates/emails/enquiry.pug', {
			transport: 'mailgun'
		}).send(enquiry, {
			apiKey: 'key-b29d8279f689cc83722167fef41ba911',
			domain: 'mail.vibez.one',
			to: 'training@vibez.one',
			from: {
				name: 'vibez.one',
				email: 'training@vibez.one'
			},
			subject: 'Booking from vibez.one'
		}, function (err, result) {
			if (err) {
				console.error('🤕 Mailgun test failed with error:\n', err);
			} else {
				console.log('📬 Successfully sent Mailgun test with result:\n', enquiry, result);
			}
		});
	});
};

Enquiry.defaultSort = '-createdAt';
Enquiry.defaultColumns = 'name, email, enquiryType, createdAt';
Enquiry.register();
