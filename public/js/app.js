$(document).foundation();

$(".owlslider").owlCarousel({
	items: 1
});
$(".recomendations-slides").owlCarousel({
	items: 1
});


$('[scroll-link]').on('click', function (e) {
	// $('body').scrollTo('section'+$(e.target).attr('href'), 150, {over: {left:0, top: -1}});
	var pos = document.querySelector($(e.target).attr('href')).offsetTop;
	var menuMargin = 156;
	$('body').scrollTo(pos - menuMargin, 150, {over: {left:0, top: -1}});
	e.preventDefault();
});
