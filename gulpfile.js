var gulp = require('gulp');
var watch = require('gulp-watch');
var shell = require('gulp-shell');
var $    = require('gulp-load-plugins')();

var sass = require('gulp-sass');

var sassPaths = [
	'./public/components/foundation-sites/scss',
	'./public/components/motion-ui/src'
];

var paths = {
	'src':['./models/**/*.js','./routes/**/*.js', 'keystone.js', 'package.json'],
	'style': {
		all: './public/scss/**/*.scss',
		output: './public/css/'
	}

};


gulp.task('watch:sass', function () {
	gulp.watch(paths.style.all, ['sass']);
});

gulp.task('orsass', function(){
	gulp.src(paths.style.all)
		.pipe(sass().on('error', sass.logError))
		.pipe(gulp.dest(paths.style.output));
});
gulp.task('sass', function() {
	return gulp.src('./public/scss/app.scss')
		.pipe($.sass({
			includePaths: sassPaths,
			outputStyle: 'compressed' // if css compressed **file size**
		})
			.on('error', $.sass.logError))
		.pipe($.autoprefixer({
			browsers: ['last 2 versions', 'ie >= 9']
		}))
		.pipe(gulp.dest(paths.style.output));
});


gulp.task('runKeystone', shell.task('node keystone.js'));
gulp.task('watch', [

  'watch:sass',

]);

gulp.task('default', ['watch', 'runKeystone']);
